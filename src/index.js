const express  = require('express');
const app = express();
const config = require('../configs/config');
const jwt = require('jsonwebtoken');
app.set('key', config.key);

app.set('port', process.env.PORT || 3001);

app.use(express.json());

app.use(require('./routes/clients'));
app.use(require('./routes/users'));
app.use(require('./routes/current'));
app.use(require('./routes/device'));
app.use(require('./routes/deviceType'));
app.use(require('./routes/sector'));
app.use(require('./routes/sensor'));
app.use(require('./routes/sensorDevice'));
app.use(require('./routes/store'));

app.listen(app.get('port'), () => {
    console.log('Run in: ', app.get('port'));
})

app.post('/auth', (req, res) => {
    if (req.body.usuario === 'kti' && req.body.contrasena === '1234') {
        const payload = {
            check: true
        };
        const token = jwt.sign(payload, app.get('key'), {
            expiresIn: 1440
        });
        res.json({
            mensaje: 'Autenticación correcta',
            token: token
        });
    } else {
        res.json({ mensaje: "Usuario o contraseña incorrectos" })
    }
});