const express  = require('express');
const router = express.Router();

const mysqlConnection = require ('../database');

router.get('/getAllSector', (req, res) => {
    var sql = 'CALL spGetAllSector()';

    mysqlConnection.query(sql, (error, rows, fields) => {
        if (!error) {
            res.status(200);
            res.json(rows[0]);
            return;
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
    mysqlConnection.end();
});

router.post('/getSectorById', (req, res) => {
    var id = req.body.id;
    var sql = 'CALL spGetSectorById(?)';
    var values = [id];

    mysqlConnection.query(sql, values, (error, rows, fields) => {
        if (!error) {
            res.status(200);
            res.json(rows[0]);
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
});

router.post('/insertSector', (req, res) => {
    var name = req.body.name;
    var level = req.body.level;
    var sectorPlan = req.body.sectorPlan;
    var store_idStore = req.body.store_idStore;
    var sql = 'CALL spInsertSector(?, ?, ?, ?);'
    var values = [name, level, sectorPlan, store_idStore];

    mysqlConnection.query(sql, values, (error, rows, fields) => {
        if (!error) {
            res.status(200).json({ response: true })
            return;
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
    mysqlConnection.end();
});

router.post('/deleteSector', (req, res) => {
    var id = req.body.id;
    var sql = 'CALL spDeleteSector(?);'
    var values = [id];

    mysqlConnection.query(sql, values, (error, rows, fields) => {
        if (!error) {
            res.status(200).json({ response: true })
            return;
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
    mysqlConnection.end();
});

module.exports = router;