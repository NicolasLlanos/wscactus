const express = require('express');
const app = express();
const router = express.Router();
const mysqlConnection = require('../database');
const config = require('../../configs/config');
const jwt = require('jsonwebtoken');

app.set('key', config.key);

router.get('/getAllClients', (req, res) => {
    const token = req.headers['access-token'];
    if (token) {
        jwt.verify(token, app.get('key'), (err, decoded) => {
            if (err) {
                return res.json({ mensaje: 'Token inválida' });
            } else {
                var sql = 'CALL spGetAllClients()';
                mysqlConnection.query(sql, (error, rows, fields) => {
                    if (!error) {
                        res.status(200);
                        res.json(rows[0]);
                        return;
                    } else {
                        res.status(404).json({ response: error })
                        return;
                    }
                });
            }
        });
    } else {
        res.send({
            mensaje: 'Token no proveída.'
        });
    }
});

router.post('/getClientsById', (req, res) => {
    const token = req.headers['access-token'];
    if (token) {
        jwt.verify(token, app.get('key'), (err, decoded) => {
            if (err) {
                return res.json({ mensaje: 'Token inválida' });
            } else {
                var id = req.body.id;
                var sql = 'CALL spGetClientsById(?)';
                var values = [id];

                mysqlConnection.query(sql, values, (error, rows, fields) => {
                    if (!error) {
                        res.status(200);
                        res.json(rows[0]);
                    } else {
                        res.status(404).json({ response: error })
                        return;
                    }
                });
            }
        });
    } else {
        res.send({
            mensaje: 'Token no proveída.'
        });
    }
});

router.post('/insertClient', (req, res) => {
    const token = req.headers['access-token'];
    if (token) {
        jwt.verify(token, app.get('key'), (err, decoded) => {
            if (err) {
                return res.json({ mensaje: 'Token inválida' });
            } else {
                var name = req.body.name;
                var rut = req.body.rut;
                var dv = req.body.dv;
                var telephone = req.body.telephone;
                var mail = req.body.mail;
                var sql = 'CALL spInsertClient(?, ?, ?, ?, ?);'
                var values = [name, rut, dv, telephone, mail];

                mysqlConnection.query(sql, values, (error, rows, fields) => {
                    if (!error) {
                        res.status(200).json({ response: true })
                        return;
                    } else {
                        res.status(404).json({ response: error })
                        return;
                    }
                });
            }
        });
    } else {
        res.send({
            mensaje: 'Token no proveída.'
        });
    }
});

router.post('/deleteClient', (req, res) => {
    const token = req.headers['access-token'];
    if (token) {
        jwt.verify(token, app.get('key'), (err, decoded) => {
            if (err) {
                return res.json({ mensaje: 'Token inválida' });
            } else {
                var id = req.body.id;
                var sql = 'CALL spDeleteClient(?);'
                var values = [id];

                mysqlConnection.query(sql, values, (error, rows, fields) => {
                    if (!error) {
                        res.status(200).json({ response: true })
                        return;
                    } else {
                        res.status(404).json({ response: error })
                        return;
                    }
                });
            }
        });
    } else {
        res.send({
            mensaje: 'Token no proveída.'
        });
    }
});

router.post('/getDeviceInClient', (req, res) => {
    const token = req.headers['access-token'];
    if (token) {
        jwt.verify(token, app.get('key'), (err, decoded) => {
            if (err) {
                return res.json({ mensaje: 'Token inválida' });
            } else {
                var id = req.body.id;
                var sql = 'CALL spGetDeviceInClient(?)';
                var values = [id];

                mysqlConnection.query(sql, values, (error, rows, fields) => {
                    if (!error) {
                        res.status(200);
                        res.json(rows[0]);
                    } else {
                        res.status(404).json({ response: error })
                        return;
                    }
                });
            }
        });
    } else {
        res.send({
            mensaje: 'Token no proveída.'
        });
    }
});


module.exports = router;