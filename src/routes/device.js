const express = require('express');
const app = express();
const router = express.Router();
const mysqlConnection = require('../database');
const config = require('../../configs/config');
const jwt = require('jsonwebtoken');

router.get('/getAllDevice', (req, res) => {
    const token = req.headers['access-token'];
    if (token) {
        jwt.verify(token, app.get('key'), (err, decoded) => {
            if (err) {
                return res.json({ mensaje: 'Token inválida' });
            } else {
                var sql = 'CALL spGetAllDevice()';
                mysqlConnection.query(sql, (error, rows, fields) => {
                    if (!error) {
                        res.status(200);
                        res.json(rows[0]);
                        return;
                    } else {
                        res.status(404).json({ response: error })
                        return;
                    }
                });
            }
        });
    } else {
        res.send({
            mensaje: 'Token no proveída.'
        });
    }
});

router.post('/getDevice', (req, res) => {
    var id = req.body.id;
    var sql = 'CALL spGetDeviceById(?)';
    var values = [id];

    mysqlConnection.query(sql, values, (error, rows, fields) => {
        if (!error) {
            res.status(200);
            res.json(rows[0]);
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
});

router.post('/insertDevice', (req, res) => {
    var name = req.body.name;
    var sensorDevice_idSensorDevice = req.body.sensorDevice_idSensorDevice;
    var deviceType_idDeviceType = req.body.deviceType_idDeviceType;
    var sql = 'CALL spInsertDevice(?, ?,?);'
    var values = [name, sensorDevice_idSensorDevice, deviceType_idDeviceType];

    mysqlConnection.query(sql, values, (error, rows, fields) => {
        if (!error) {
            res.status(200).json({ response: true })
            return;
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
});

router.post('/deleteDevice', (req, res) => {
    var id = req.body.id;
    var sql = 'CALL spDeleteDevice(?);'
    var values = [id];

    mysqlConnection.query(sql, values, (error, rows, fields) => {
        if (!error) {
            res.status(200).json({ response: true })
            return;
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
});

module.exports = router;