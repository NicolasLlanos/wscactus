const express = require('express');
const app = express();
const router = express.Router();
const mysqlConnection = require('../database');
const config = require('../../configs/config');
const jwt = require('jsonwebtoken');

app.set('key', config.key);

router.get('/getAllSensor', (req, res) => {
    const token = req.headers['access-token'];
    if (token) {
        jwt.verify(token, app.get('key'), (err, decoded) => {
            if (err) {
                return res.json({ mensaje: 'Token inválida' });
            } else {
                var sql = 'CALL spGetAllSensor()';

                mysqlConnection.query(sql, (error, rows, fields) => {
                    if (!error) {
                        res.status(200);
                        res.json(rows[0]);
                        return;
                    } else {
                        res.status(404).json({ response: error })
                        return;
                    }
                });
            }
        });
    } else {
        res.send({
            mensaje: 'Token no proveída.'
        });
    }
});

router.post('/getSensorById', (req, res) => {
    const token = req.headers['access-token'];
    if (token) {
        jwt.verify(token, app.get('key'), (err, decoded) => {
            if (err) {
                return res.json({ mensaje: 'Token inválida' });
            } else {
                var id = req.body.id;
                var sql = 'CALL spGetSensorById(?)';
                var values = [id];

                mysqlConnection.query(sql, values, (error, rows, fields) => {
                    if (!error) {
                        res.status(200);
                        res.json(rows[0]);
                    } else {
                        res.status(404).json({ response: error })
                        return;
                    }
                });
            }
        });
    } else {
        res.send({
            mensaje: 'Token no proveída.'
        });
    }
});

router.post('/insertSensor', (req, res) => {
    const token = req.headers['access-token'];
    if (token) {
        jwt.verify(token, app.get('key'), (err, decoded) => {
            if (err) {
                return res.json({ mensaje: 'Token inválida' });
            } else {
                var isOn = req.body.isOn;
                var sector_idSector = req.body.sector_idSector;
                var sql = 'CALL spInsertSensor(?, ?);'
                var values = [isOn, sector_idSector];

                mysqlConnection.query(sql, values, (error, rows, fields) => {
                    if (!error) {
                        res.status(200).json({ response: true })
                        return;
                    } else {
                        res.status(404).json({ response: error })
                        return;
                    }
                });
            }
        });
    } else {
        res.send({
            mensaje: 'Token no proveída.'
        });
    }
});
router.post('/deleteSensor', (req, res) => {
    const token = req.headers['access-token'];
    if (token) {
        jwt.verify(token, app.get('key'), (err, decoded) => {
            if (err) {
                return res.json({ mensaje: 'Token inválida' });
            } else {
                var id = req.body.id;
                var sql = 'CALL spDeleteSensor(?);'
                var values = [id];

                mysqlConnection.query(sql, values, (error, rows, fields) => {
                    if (!error) {
                        res.status(200).json({ response: true })
                        return;
                    } else {
                        res.status(404).json({ response: error })
                        return;
                    }
                });
            }
        });
    } else {
        res.send({
            mensaje: 'Token no proveída.'
        });
    }
});

router.post('/getClientOfDevice', (req, res) => {
    const token = req.headers['access-token'];
    if (token) {
        jwt.verify(token, app.get('key'), (err, decoded) => {
            if (err) {
                return res.json({ mensaje: 'Token inválida' });
            } else {
                var id = req.body.id;
                var sql = 'CALL spGetClientOfDevice(?)';
                var values = [id];

                mysqlConnection.query(sql, values, (error, rows, fields) => {
                    if (!error) {
                        res.status(200);
                        res.json(rows[0]);
                    } else {
                        res.status(404).json({ response: error })
                        return;
                    }
                });
            }
        });
    } else {
        res.send({
            mensaje: 'Token no proveída.'
        });
    }
});
module.exports = router;