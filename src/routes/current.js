const express  = require('express');
const router = express.Router();

const mysqlConnection = require ('../database');

router.get('/getAllCurrent', (req, res) => {
    var sql = 'CALL spGetAllCurrent()';

    mysqlConnection.query(sql, (error, rows, fields) => {
        if (!error) {
            res.status(200);
            res.json(rows[0]);
            return;
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
    mysqlConnection.end();
});

router.post('/getCurrentById', (req, res) => {
    var id = req.body.id;
    var sql = 'CALL spGetCurrentById(?)';
    var values = [id];

    mysqlConnection.query(sql, values, (error, rows, fields) => {
        if (!error) {
            res.status(200);
            res.json(rows[0]);
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
});

router.post('/insertCurrent', (req, res) => {
    var timestamp = req.body.timestamp;
    var current = req.body.current;
    var sensor_idSensor = req.body.sensor_idSensor;
    var sql = 'CALL spInsertCurrent(?, ?, ?);'
    var values = [timestamp, current, sensor_idSensor];

    mysqlConnection.query(sql, values, (error, rows, fields) => {
        if (!error) {
            res.status(200).json({ response: true })
            return;
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
    mysqlConnection.end();
});

router.post('/deleteCurrent', (req, res) => {
    var id = req.body.id;
    var sql = 'CALL spDeleteCurrent(?);'
    var values = [id];

    mysqlConnection.query(sql, values, (error, rows, fields) => {
        if (!error) {
            res.status(200).json({ response: true })
            return;
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
    mysqlConnection.end();
});

module.exports = router;