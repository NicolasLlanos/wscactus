const express  = require('express');
const router = express.Router();

const mysqlConnection = require ('../database');

router.get('/getAllUsers', (req, res) => {
    var sql = 'CALL spGetAllUsers()';

    mysqlConnection.query(sql, (error, rows, fields) => {
        if (!error) {
            res.status(200);
            res.json(rows[0]);
            return;
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
    mysqlConnection.end();
});

router.post('/getUsersById', (req, res) => {
    var id = req.body.id;
    var sql = 'CALL spGetUsersById(?)';
    var values = [id];

    mysqlConnection.query(sql, values, (error, rows, fields) => {
        if (!error) {
            res.status(200);
            res.json(rows[0]);
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
});

router.post('/insertUser', (req, res) => {
    var name = req.body.name;
    var rut = req.body.rut;
    var dv = req.body.dv;
    var telephone = req.body.telephone;
    var mail = req.body.mail;
    var sql = 'CALL spInsertUser(?, ?, ?, ?, ?);'
    var values = [name, rut, dv, telephone, mail];

    mysqlConnection.query(sql, values, (error, rows, fields) => {
        if (!error) {
            res.status(200).json({ response: true })
            return;
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
    mysqlConnection.end();
});

router.post('/deleteUser', (req, res) => {
    var id = req.body.id;
    var sql = 'CALL spDeleteUser(?);'
    var values = [id];

    mysqlConnection.query(sql, values, (error, rows, fields) => {
        if (!error) {
            res.status(200).json({ response: true })
            return;
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
    mysqlConnection.end();
});

router.post('/login', (req, res) => {
    var email = req.body.email;
    var password = req.body.password;
    var sql = 'CALL spLoginUser(?,?);'
    var values = [email, password];
    

    mysqlConnection.query(sql, values, (error, rows, fields) => {
        if (!error) {
            res.status(200);
            res.json(rows[0]);
            return;
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
 });

module.exports = router;