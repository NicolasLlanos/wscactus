const express  = require('express');
const router = express.Router();

const mysqlConnection = require ('../database');

router.get('/getAllDeviceType', (req, res) => {
    var sql = 'CALL spGetAllDeviceType()';

    mysqlConnection.query(sql, (error, rows, fields) => {
        if (!error) {
            res.status(200);
            res.json(rows[0]);
            return;
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
    mysqlConnection.end();
});

router.post('/getDeviceTypeById', (req, res) => {
    var id = req.body.id;
    var sql = 'CALL spGetDeviceTypeById(?)';
    var values = [id];

    mysqlConnection.query(sql, values, (error, rows, fields) => {
        if (!error) {
            res.status(200);
            res.json(rows[0]);
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
});

router.post('/insertDeviceTyoe', (req, res) => {
    var name = req.body.name;
    var icon = req.body.icon;
    var sql = 'CALL spInsertDeviceType(?, ?);'
    var values = [name, icon];

    mysqlConnection.query(sql, values, (error, rows, fields) => {
        if (!error) {
            res.status(200).json({ response: true })
            return;
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
    mysqlConnection.end();
});

router.post('/deleteDeviceType', (req, res) => {
    var id = req.body.id;
    var sql = 'CALL spDeleteDeviceType(?);'
    var values = [id];

    mysqlConnection.query(sql, values, (error, rows, fields) => {
        if (!error) {
            res.status(200).json({ response: true })
            return;
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
    mysqlConnection.end();
});

module.exports = router;