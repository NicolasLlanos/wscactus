const express = require('express');
const router = express.Router();

const mysqlConnection = require('../database');

router.get('/getAllStore', (req, res) => {
    var sql = 'CALL spGetAllStore()';

    mysqlConnection.query(sql, (error, rows, fields) => {
        if (!error) {
            res.status(200);
            res.json(rows[0]);
            return;
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
    mysqlConnection.end();
});

router.post('/getStoreById', (req, res) => {
    var id = req.body.id;
    var sql = 'CALL spGetStoreByID(?)';
    var values = [id];

    mysqlConnection.query(sql, values, (error, rows, fields) => {
        if (!error) {
            res.status(200);
            res.json(rows[0]);
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
});

router.post('/insertStore', (req, res) => {
    var name = req.body.name;
    var address = req.body.address;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var client_idClient = req.body.client_idClient;
    var sql = 'CALL spInsertStore(?, ?, ?, ?, ?);'
    var values = [name, address, latitude, longitude, client_idClient];

    mysqlConnection.query(sql, values, (error, rows, fields) => {
        if (!error) {
            res.status(200).json({ response: true })
            return;
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
    mysqlConnection.end();
});

router.post('/deleteStore', (req, res) => {
    var id = req.body.id;
    var sql = 'CALL spDeleteStore(?);'
    var values = [id];

    mysqlConnection.query(sql, values, (error, rows, fields) => {
        if (!error) {
            res.status(200).json({ response: true })
            return;
        } else {
            res.status(404).json({ response: error })
            return;
        }
    });
    mysqlConnection.end();
});

module.exports = router;